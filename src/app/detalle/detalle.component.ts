import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PersonasService } from '../services/personas.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html'
})
export class DetalleComponent {
  id = null;
  persona: any = {};
  constructor (private route: ActivatedRoute, private personasService: PersonasService) {
    console.log(this.route.snapshot.params['id']);
    console.log(this.route.queryParams);
    console.log(this.route.snapshot.queryParams['action']);
    console.log(this.route.snapshot.queryParams['refer']);
    this.id = this.route.snapshot.params['id'];
    this.persona = this.personasService.buscarPersona(this.id);
  }
}

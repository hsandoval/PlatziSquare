import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { ResaltarDirective } from './directives/app.resaltar.directives';
import { ContarClicksDirective } from './directives/app.contarClicks.directives';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule} from '@angular/router';
import { DetalleComponent } from './detalle/detalle.component';
import { PersonasComponent } from './personas/personas.component';
import { ContactoComponent } from './contacto/contacto.component';
import { PersonasService } from './services/personas.service';
<<<<<<< HEAD
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { CrearComponent } from './crear/crear.component';
=======
import { CrearComponent } from './crear/crear.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
>>>>>>> Development

const appRoutes: Routes = [
  {path: '', component: PersonasComponent},
  {path: 'personas', component: PersonasComponent},
  {path: 'detalle/:id', component: DetalleComponent},
  {path: 'contacto', component: ContactoComponent},
  {path: 'crear', component: CrearComponent}
];

export const firebaseConfig = {
  apiKey: 'AIzaSyBaTKDdi9BckZ9wYJusnuU0HEqc3sEGhu0',
  authDomain: 'platzisquare-1511140826912.firebaseapp.com',
  databaseURL: 'https://platzisquare-1511140826912.firebaseio.com',
  storageBucket: 'platzisquare-1511140826912.appspot.com',
  messagingSenderId: '850378892538'
};

@NgModule({
  declarations: [
    AppComponent,
    ResaltarDirective,
    ContarClicksDirective,
    PersonasComponent,
    DetalleComponent,
    ContactoComponent,
    CrearComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAOynpVDiRujF8hUEM8ZiaqG-jpCfN2t-E'
    }),
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig),
<<<<<<< HEAD
    AngularFireDatabase,
=======
    AngularFireDatabaseModule,
>>>>>>> Development
    AngularFireAuthModule
  ],
  providers: [PersonasService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Directive, OnInit, ElementRef, Renderer2, Input} from '@angular/core';

@Directive({
  selector: '[appResaltar]'
})
export class ResaltarDirective implements OnInit {
  constructor(private elRef: ElementRef, private rendered: Renderer2) {}
  @Input('appResaltar') plan: string = '';
  ngOnInit() {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    if (this.plan === 'Pagado') {
      this.rendered.setStyle(this.elRef.nativeElement, 'background-color', 'yellow');
      this.rendered.setStyle(this.elRef.nativeElement, 'font-weight', 'bold');
    }
  }
}

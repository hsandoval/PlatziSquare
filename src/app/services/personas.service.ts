import { Injectable } from '@angular/core';

@Injectable()

export class PersonasService {
  personas: any = [
    {id: 1, plan: 'Pagado', nombre: 'Brat Pitt', cercania: 1, distancia: 1, active: true},
    {id: 2, plan: 'Pagado', nombre: 'Angeline Jolie', cercania: 2, distancia: 1.4, active: true},
    {id: 3, plan: 'Gratuito', nombre: 'Nicolage Cage', cercania: 2, distancia: 16, active: true},
    {id: 4, plan: 'Pagado', nombre: 'Matt Damon', cercania: 3, distancia: 1.3, active: true},
    {id: 5, plan: 'Gratuito', nombre: 'The rock', cercania: 1, distancia: 0.5, active: false},
    {id: 6, plan: 'Gratuito', nombre: 'Zack Efrom', cercania: 4, distancia: 42, active: true}
  ];
  public getPersonas() {
    return this.personas;
  }
  buscarPersona(id) {
    // return this.personas.filter((persona) => { return persona.id == this.id })[0] || null;
    // tslint:disable-next-line:triple-equals
    return this.personas.filter((persona) => persona.id == id)[0] || null;
  }
}

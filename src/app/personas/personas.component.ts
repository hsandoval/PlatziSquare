import { Component } from '@angular/core';
import { PersonasService } from '../services/personas.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html'
})
export class PersonasComponent {
  title = 'PlatziSquare';
  lat: number = 10.5013628;
  lng: number = -66.9748801;
  personas = null;
  constructor( private personasService: PersonasService) {
    this.personas = personasService.getPersonas();
  }
}
